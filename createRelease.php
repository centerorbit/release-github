<?php
/**
 * Created by PhpStorm.
 * User: centerorbit
 * Date: 8/11/18
 * Time: 2:52 PM
 */

require 'vendor/autoload.php';

use GuzzleHttp\Client;


$test = getenv ( "TEST");

$tag = getenv ( "TAG");
$owner = getenv ( "OWNER");
$repo = getenv ( "REPO");
$accessToken = getenv ( "ACCESS_TOKEN");
$prerelease = getenv ( "PRE_RELEASE");


if($test) {
    echo "Release Test Successful \n";
    exit;
}

$apiClient = new Client([
    'base_uri' => 'https://api.github.com',
    'timeout'  => 5.0,
]);

try {
    $response = $apiClient->post("repos/{$owner}/{$repo}/releases",
        [
            'query' => ['access_token' => $accessToken],
            'json' => [
                'tag_name' => $tag,
                'target_commitish' => 'master',
                'name' => $tag,
    //            'body'=> 'Description of the release',
                'draft' => false,
                'prerelease' => ((bool)$prerelease),
            ]
        ]
    );
}
catch (Exception $e){
    echo "Couldn't create a Github release for {$tag}.";
    return -1;
}

$results = json_decode($response->getBody());
$uploadUrl = substr($results->upload_url, 0, strpos($results->upload_url,'{'));

echo $uploadUrl;