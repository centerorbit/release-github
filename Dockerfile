FROM php:7.2-cli

RUN apt-get update && apt-get install -y \
    zip \
    unzip \
    zlib1g-dev \
    libcurl4-gnutls-dev \
    curl \
    git

RUN docker-php-ext-install curl
RUN docker-php-ext-install zip

# Install composer
COPY ./docker/composer_install.sh /
RUN ["chmod", "u+x", "/composer_install.sh"]
RUN /composer_install.sh
RUN mv composer.phar /usr/local/bin/composer
RUN rm /composer_install.sh

COPY . /var/code
WORKDIR /var/code