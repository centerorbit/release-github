<?php
/**
 * Created by PhpStorm.
 * User: centerorbit
 * Date: 8/11/18
 * Time: 2:52 PM
 */

require 'vendor/autoload.php';

use GuzzleHttp\Client;


$test = getenv ( "TEST");

$uploadUrl = getenv ( "UPLOAD_URL");
$accessToken = getenv ( "ACCESS_TOKEN");
$name = getenv ( "RELEASE_NAME");

if($test) {
    echo "Binary Test Successful \n";
    exit;
}

if (strpos($uploadUrl, "https") === false){
    echo "{$uploadUrl} is an invalid Upload URL";
    exit -1;
}


$uploadClient = new Client([
    'base_uri' =>  $uploadUrl,
    'timeout'  => 5.0,
]);

//// Provide an fopen resource.
$zipFile = fopen($name.'.zip', 'r');

try {
    $response = $uploadClient->post($uploadUrl,
        [
            'header' => ['Content-Type'=> 'application/zip'],
            'query' => [
                'access_token' => $accessToken,
                'name' => $name.'.zip'
            ],
            'body' => $zipFile
        ]
    );
} catch (Exception $e){
    echo "Couldn't upload a release binary of '{$name}' for '{$uploadUrl}'";
    exit -1;
}


